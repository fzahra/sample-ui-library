// @ts-nocheck
import React from "react";
import styled from 'styled-components';


const color = {
  solid: "#fff",
  outline: "#6A69E4"
}
const bgColor = {
  solid: "#6A69E4",
  outline: "#fff"
}

const getTheme = (props) => {
  const { foreground, background, variant} = props;
  const theme = {foreground: foreground || "#fff", background: background || "#6A69E4", border: background || "#6A69E4"};
  if(variant === "outline") {
    const dump = {...theme};
    theme.foreground = dump.background;
    theme.background = dump.foreground;
  }
  return theme;
}

const Container = styled.button`
border-radius: 4px;
border: 1px solid ${props => getTheme(props).border};
background: ${props => getTheme(props).background};
letter-spacing: 1px;
font-size: 16px;
color: ${props => getTheme(props).foreground};
padding: 0.5rem 3rem;
height: 45px;
font-weight: normal
`;

export const Button = ({label, variant, foreground, background}) => {
  return <Container variant={variant || "solid"} foreground={foreground} background={background} >{label}</Container>
}

