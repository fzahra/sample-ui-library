// @ts-nocheck
import React from "react";
import styled from 'styled-components';


const colors = {
  primary: "#6A69E4",
  secondary: "#841010"
}

const Container = styled.button`
border-radius: 4px;
border: transparent;
color: ${props => colors[props.variant]};
letter-spacing: 1px;
font-size: 16px;
background: #fff;
padding: 0.5rem 3rem;
height: 45px;
font-weight: normal
`;

export const Button = ({label, variant}) => {
  return <Container variant={variant || "primary"}>{label}</Container>
}

