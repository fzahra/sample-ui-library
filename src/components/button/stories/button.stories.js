import { BADGE } from '@geometricpanda/storybook-addon-badges';
import React from "react";
import { Button } from "../button";

export default {
    title: "Indigo/Button",
    argTypes: {
    variant: { control: 'radio', options: ['solid', 'outline'] },
    label: { control: 'text', defaultValue: "Login" },
    foreground: { control: 'color' },
    background: { control: 'color' },
    parameters: {
    badges: [BADGE.DEPRECATED, BADGE.OBSOLETE]
  }
  },
}

export const Solid = (args) => <Button {...args} />;
Solid.args = {};