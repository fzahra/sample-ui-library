"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Button = void 0;

var _taggedTemplateLiteral2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/taggedTemplateLiteral"));

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

var colors = {
  primary: "#6A69E4",
  secondary: "#841010"
};

var Container = _styledComponents.default.button(_templateObject || (_templateObject = (0, _taggedTemplateLiteral2.default)(["\nborder-radius: 4px;\nborder: transparent;\ncolor: ", ";\nletter-spacing: 1px;\nfont-size: 16px;\nbackground: #fff;\npadding: 0.5rem 3rem;\nheight: 45px;\nfont-weight: normal\n"])), function (props) {
  return colors[props.variant];
});

var Button = function Button(_ref) {
  var label = _ref.label,
      variant = _ref.variant;
  return /*#__PURE__*/_react.default.createElement(Container, {
    variant: variant || "primary"
  }, label);
};

exports.Button = Button;