"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Button = void 0;

var _taggedTemplateLiteral2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/taggedTemplateLiteral"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/esm/objectSpread2"));

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

var color = {
  solid: "#fff",
  outline: "#6A69E4"
};
var bgColor = {
  solid: "#6A69E4",
  outline: "#fff"
};

var getTheme = function getTheme(props) {
  var foreground = props.foreground,
      background = props.background,
      variant = props.variant;
  var theme = {
    foreground: foreground || "#fff",
    background: background || "#6A69E4",
    border: background || "#6A69E4"
  };

  if (variant === "outline") {
    var dump = (0, _objectSpread2.default)({}, theme);
    theme.foreground = dump.background;
    theme.background = dump.foreground;
  }

  return theme;
};

var Container = _styledComponents.default.button(_templateObject || (_templateObject = (0, _taggedTemplateLiteral2.default)(["\nborder-radius: 4px;\nborder: 1px solid ", ";\nbackground: ", ";\nletter-spacing: 1px;\nfont-size: 16px;\ncolor: ", ";\npadding: 0.5rem 3rem;\nheight: 45px;\nfont-weight: normal\n"])), function (props) {
  return getTheme(props).border;
}, function (props) {
  return getTheme(props).background;
}, function (props) {
  return getTheme(props).foreground;
});

var Button = function Button(_ref) {
  var label = _ref.label,
      variant = _ref.variant,
      foreground = _ref.foreground,
      background = _ref.background;
  return /*#__PURE__*/_react.default.createElement(Container, {
    variant: variant || "solid",
    foreground: foreground,
    background: background
  }, label);
};

exports.Button = Button;