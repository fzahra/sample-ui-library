"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Solid = exports.default = void 0;

var _storybookAddonBadges = require("@geometricpanda/storybook-addon-badges");

var _react = _interopRequireDefault(require("react"));

var _button = require("../button");

var _default = {
  title: "Indigo/Button",
  argTypes: {
    variant: {
      control: 'radio',
      options: ['solid', 'outline']
    },
    label: {
      control: 'text',
      defaultValue: "Login"
    },
    foreground: {
      control: 'color'
    },
    background: {
      control: 'color'
    },
    parameters: {
      badges: [_storybookAddonBadges.BADGE.DEPRECATED, _storybookAddonBadges.BADGE.OBSOLETE]
    }
  }
};
exports.default = _default;

var Solid = function Solid(args) {
  return /*#__PURE__*/_react.default.createElement(_button.Button, args);
};

exports.Solid = Solid;
Solid.args = {};