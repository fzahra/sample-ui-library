"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Primary = exports.default = void 0;

var _Button = require("./Button");

var _default = {
  title: 'Example/Sample',
  component: _Button.Button,
  argTypes: {
    backgroundColor: {
      control: 'color'
    }
  }
};
exports.default = _default;

var Template = function Template(args) {
  return /*#__PURE__*/React.createElement(_Button.Button, args);
};

var Primary = Template.bind({});
exports.Primary = Primary;
Primary.args = {
  primary: true,
  label: 'Button'
}; // export const Secondary = Template.bind({});
// Secondary.args = {
//   label: 'Button',
// };
// export const Large = Template.bind({});
// Large.args = {
//   size: 'large',
//   label: 'Button',
// };
// export const Small = Template.bind({});
// Small.args = {
//   size: 'small',
//   label: 'Button',
// };